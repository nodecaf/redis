# Nodecaf Redis Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v0.1.0] - 2022-03-02

### Changed
- interface to use [redis](https://github.com/redis/node-redis) uppercase or camel-case commands

## [v0.0.4] - 2021-09-11

### Added
- support for array of channels on subscribe client

### Fixed
- init process not waiting for subscribes to happen

### Changed
- clients to crash on unexpected connection close

## [v0.0.3] - 2021-07-14

### Fixed
- bug running `SUBSCRIBE` even when failed to connect

## [v0.0.2] - 2021-07-14

### Fixed
- Initialization logic to properly cleanup event listeners

## [v0.0.1] - 2020-12-10
- First officially published version.

[v0.0.1]: https://gitlab.com/nodecaf/redis/-/tags/v0.0.1
[v0.0.2]: https://gitlab.com/nodecaf/redis/-/tags/v0.0.2
[v0.0.3]: https://gitlab.com/nodecaf/redis/-/tags/v0.0.3
[v0.0.4]: https://gitlab.com/nodecaf/redis/-/tags/v0.0.4
[v0.1.0]: https://gitlab.com/nodecaf/redis/-/tags/v0.1.0
