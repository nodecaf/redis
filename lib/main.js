
const { createClient } = require('redis');

function initClient(conf){
    const client = createClient({
        socket: conf,
        ...conf,
        disableOfflineQueue: true
    });

    client.on('error', err => {
        client.disconnect();
        throw err;
    });

    // return new Proxy(client, {
    //     get(target, cmd){
    //         return target[cmd.toUpperCase()];
    //     }
    // });

    return client;
}

module.exports = async function(conf, sub){

    conf = conf || {};

    const client = initClient(conf);

    client.close = function(){
        return Promise.all([
            client.quit(),
            client.sub?.quit()
        ]);
    };

    if(sub){
        client.sub = initClient(conf);
        await client.sub.connect();
    }

    await client.connect();

    return client;
}
