/* eslint-env mocha */

const assert = require('assert');

const redis = require('../lib/main');

const REDIS_HOST = process.env.REDIS_HOST || 'localhost';

describe('Nodecaf Redis', () => {

    it('Should create a redis client', async function(){
        const myr = await redis({ host: REDIS_HOST, port: '6379' });
        await myr.set('abc', '23');
        assert.strictEqual(await myr.get('abc'), '23');
        await myr.close();
    });

    it('Should create a surrogate subscribe mode client', function(done){
        (async function(){
            const myr = await redis({ host: REDIS_HOST, port: '6379' }, true);
            await myr.sub.subscribe('foo-chan', async function(){
                await myr.close();
                done();
            })
            myr.publish('foo-chan', 'here');
        })();
    });

    // it('Should crash on connection aborted', async function(){
    //     const proxy = require('node-tcp-proxy');
    //     const newProxy = proxy.createProxy(6000, REDIS_HOST, 6379);

    //     const myr = await redis({ host: 'localhost', port: '6000' });
    //     const p = new Promise(done => myr.on('error', done));



    //     // await myr.quit();
    //     await myr.set('a', 1);
    //     // console.log(await myr.get('a'));

    //     // newProxy.end();
    //     newProxy.end();

    //     await p;

    // });

});
